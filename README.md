<h1>What is it?</h1>
This is a test application for comparing different approaches for Multitasking in .Net

<h1>What is it for?</h1>
You may configure different multitask approaches, mesure perfomence of each and compare them.<br/>
This may help you to pick configuration and method for best perfomence.

<h1>How to use it?</h1>
Check out Wiki - https://gitlab.com/hotfury/Multitask/wikis/home