﻿using System.Collections.Generic;

namespace MultiTaskTest.Visualization.Models
{
    public class ThreadMeasuresViewModel
    {
        public bool ShowThreadRace { get; set; }

        public IList<ThreadMetricViewModel> ThreadMetricViewModels { get; set; }

        public Dictionary<string, int> ComputationStatistic { get; set; }

        public long ElapsedTime { get; set; }

        public int ThreadsCount { get; set; }

        public ThreadMeasuresViewModel()
        {
            ThreadMetricViewModels = new List<ThreadMetricViewModel>();
        }
    }
}