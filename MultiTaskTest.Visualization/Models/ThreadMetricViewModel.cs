﻿using System.Collections.Generic;
using MultiTaskTest.Common.Statistic;

namespace MultiTaskTest.Visualization.Models
{
    public class ThreadMetricViewModel
    {
        public int ThreadId { get; set; }

        public string ThreadName { get; set; }

        public bool IsThreadFromPool { get; set; }

        public IList<TimeStamp> TimeBetweenIterations { get; set; }
    }
}