﻿(() => {
    let selectors = {
        tab: '[data-tab]',
        activeTab: '.active>[data-tab]',
        tabContent: '[data-tab-content="{{tab}}"]',
        runButtion: '[data-button-runapplication]',
        commonForm: '#commonSettings',
        extendForm: '[data-from-settings-extended="{{activeTab}}"]',
        animationBlock: '[data-tab-animation="{{activeTab}}"]',
        loader: '[data-amimation-loader="{{activeTab}}"]',
        clearLogsButton: '[data-button-clearlogs]',
        start: '[data-start]',
        threadTiming: '[data-thread-timing]'
    };

    let classes = {
        active: 'active',
        hidden: 'hidden',
        displayNone: 'display-none'
    };

    $(selectors.runButtion).on('click', event => {
        let url = $(event.currentTarget).data('url');
        let commonSettingsData = $(selectors.commonForm).serialize();
        let activeTab = $(selectors.activeTab).data('tab');
        let extendForm = selectors.extendForm.replace('{{activeTab}}', activeTab);
        let extendSettingsData = $(extendForm).serialize();
        let postBody = commonSettingsData + '&' + extendSettingsData;
        let animationBlock = selectors.animationBlock.replace('{{activeTab}}', activeTab);
        let loader = selectors.loader.replace('{{activeTab}}', activeTab);

        $(loader).removeClass(classes.hidden);
        $(animationBlock).addClass(classes.hidden);
        $.post(url, postBody, (response) => {
            $(loader).addClass(classes.hidden);
            $(animationBlock).html(response);
            $(animationBlock).removeClass(classes.hidden);
            runAnimation(activeTab);
        });
    });

    $(selectors.clearLogsButton).on('click', event => {
        let url = $(event.target).data('url');
        let logSelectors = {
            logsContainer: '[data-logs-container]',
            emptyLogs: '[data-logs-empty]'
        };

        if (confirm("You sure?")) {
            $.post(url).success(() => {
                $(logSelectors.logsContainer).addClass(classes.displayNone);
                $(logSelectors.emptyLogs).removeClass(classes.displayNone);
                $(selectors.clearLogsButton).addClass(classes.displayNone);
                alert("Logs erased");
            });
        }
    });

    $(selectors.tab).on('click', event => {
        let $this = $(event.target);
        if ($this.parent().hasClass(classes.active)) {
            return;
        }

        let currentTabContent = getTabContentSelector($this);
        let activeTabContent = getTabContentSelector($(selectors.activeTab));

        $(selectors.activeTab).parent().removeClass(classes.active);
        $this.parent().addClass(classes.active);

        $(currentTabContent).removeClass(classes.hidden);
        $(currentTabContent).attr('data-tab-content-active', true);

        $(activeTabContent).addClass(classes.hidden);
        $(activeTabContent).attr('data-tab-content-active', false);
    });

    function getTabContentSelector($tab) {
        return selectors.tabContent.replace('{{tab}}', $tab.data('tab'));
    }

    function runAnimation(activeTab) {
        let animationSelectors = {
            threads: '[data-tab-content="' + activeTab + '"] [data-thread-name]',
            threadCompleted: '[data-tab-content="' + activeTab + '"] [data-thread-completed="{{threadId}}"]',
            iterationThread: '[data-tab-content="' + activeTab + '"] [data-iteration-thread="{{threadName}}"]',
            iterationsMax: '[data-tab-content="' + activeTab + '"] [data-iterations-max]',
            activeAction: '[data-tab-content="' + activeTab + '"] [data-action-active]',
            actionFromPool: '[data-tab-content="' + activeTab + '"] [data-action-pool][data-actionid="{{actionId}}"]',
            currentAction: '[data-tab-content="' + activeTab + '"] [data-action-current="{{actionId}}"]'
        };

        let calculationPathWidth = $(animationSelectors.threads).width() - 65;
        let iterationWidth = calculationPathWidth / $(animationSelectors.iterationsMax).data('iterationsMax');
        $(selectors.start).css('left', (iterationWidth + 50) + 'px');

        let $activeActions = $(animationSelectors.activeAction);
        
        for (let i = 0; i < $activeActions.length; i++) {
            let $currentAction = $($activeActions[i]);
            let imageSelector = animationSelectors.actionFromPool.replace('{{actionId}}', $currentAction.data('actionid'));
            let $actionImage = $(imageSelector);
            $actionImage.attr('data-action-current', $currentAction.data('actionid'));
            $currentAction.prepend($actionImage);
        }

        let threads = $(animationSelectors.threads);
        createGrid(25, threads.length);

        let iterationAnimationFunctions = new Array();

        for (let i = 0; i < threads.length; i++) {
            let threadName = $(threads[i]).data('threadName');
            let iterationBlockSelector = animationSelectors.iterationThread.replace('{{threadName}}', threadName);
            let $threadIterationBlocks = $(iterationBlockSelector);
            let threadIterationBlocksCount = $threadIterationBlocks.length;
            let threadIterationFunctions = new Array();

            let lastBlockAnimateFunction = () => {
                let $lastElement = $($threadIterationBlocks[threadIterationBlocksCount - 1]);
                let iterationTime = $lastElement.data('iterationTime');
                $lastElement.animate({ width: iterationWidth + 'px' },
                    iterationTime,
                    'linear',
                    () => {
                        let threadId = animationSelectors.threadCompleted.replace('{{threadId}}', threadName);
                        $(threadId).removeClass(classes.displayNone);
                    });
            };
            threadIterationFunctions.push(lastBlockAnimateFunction);

            for (let j = threadIterationBlocksCount - 2; j >= 0; j--) {
                let previousFunction = threadIterationFunctions.pop();
                let currentBlockAnimateFunction = () => {
                    let $current = $($threadIterationBlocks[j]);
                    let $previous = $($threadIterationBlocks[j - 1]);
                    if ($previous.length && $current.data('iterationAction') !== $previous.data('iterationAction')) {
                        let imageSelector = animationSelectors.actionFromPool.replace('{{actionId}}', $current.data('iterationAction'));
                        let $actionImage = $(imageSelector);
                        let currentActionSelector =
                            animationSelectors.currentAction.replace('{{actionId}}', $previous.data('iterationAction'));
                        $actionImage.attr('data-action-current', $current.data('iterationAction'));
                        $(currentActionSelector).parent().prepend($actionImage);
                        $current.append($(currentActionSelector));
                    }
                    let iterationTime = $current.data('iterationTime');
                    $current.animate({ width: iterationWidth + 'px' }, iterationTime, 'linear', previousFunction);
                };
                threadIterationFunctions.push(currentBlockAnimateFunction);
            }

            iterationAnimationFunctions.push(threadIterationFunctions.pop());
        }

        for (let i = 0; i < iterationAnimationFunctions.length; i++) {
            iterationAnimationFunctions[i]();
        }
    }

    function createGrid(size, threadLength) {
        $(selectors.threadTiming).css('height', threadLength * 27 + 'px');
        var ratioW = Math.floor($(selectors.threadTiming).width() / size),
            ratioH = Math.floor($(selectors.threadTiming).height() / size);

        var parent = $('<div />', {
            class: 'grid',
            width: ratioW * size,
            height: ratioH * size
        }).addClass('grid').appendTo($(selectors.threadTiming));

        for (var i = 0; i < ratioH; i++) {
            for (var p = 0; p < ratioW; p++) {
                $('<div />', {
                    width: size - 1,
                    height: size - 1
                }).appendTo(parent);
            }
        }
    }
})();