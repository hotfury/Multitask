﻿using Microsoft.AspNetCore.Mvc;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Statistic;

namespace MultiTaskTest.Visualization.Controllers
{
    [Produces("application/json")]
    [Route("api/app")]
    public class ApplicationController : Controller
    {
        private readonly ApplicationMap _applicationMap = new ApplicationMap(Constants.WebApplicationType);

        [HttpPost("run")]
        public IActionResult Run(ApplicationSettings settings)
        {
            ThreadsMeasures threadsMeasures = _applicationMap.Applications[settings.ApplicationName].Main(settings);

            return Json(threadsMeasures);
        }
    }
}