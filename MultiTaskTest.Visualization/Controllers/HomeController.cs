﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Visualization.Builders;
using MultiTaskTest.Visualization.Models;

namespace MultiTaskTest.Visualization.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationMap _applicationMap = new ApplicationMap(Constants.WebApplicationType);

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Run(ApplicationSettings settings)
        {
            ThreadsMeasures threadsMeasures = _applicationMap.Applications[settings.ApplicationName].Main(settings);

            ThreadMeasuresViewModel threadMeasuresViewModel = ThreadMeasuresViewModelBuilder
                .CreateFrom(threadsMeasures)
                .WithShowThreadRace(settings.ShowThreadRace)
                .WithThreadMetrics()
                .Model();

            return PartialView(threadMeasuresViewModel);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}