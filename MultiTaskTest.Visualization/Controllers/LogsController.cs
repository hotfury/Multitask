﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Constants = MultiTaskTest.Common.Constants;

namespace MultiTaskTest.Visualization.Controllers
{
    public class LogsController : Controller
    {
        public IActionResult Index()
        {
            var directory = new DirectoryInfo(Constants.ExcelLogDirectory);

            IEnumerable<string> fileNames = directory.Exists
                ? directory.GetFiles().Select(x => x.Name)
                : Enumerable.Empty<string>();
            
            return View(fileNames);
        }

        public IActionResult Download(string fileName)
        {
            string filePath = Path.Combine(Constants.ExcelLogDirectory, fileName);
            byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Text.Xml, fileName);
        }

        [HttpPost]
        public IActionResult Clear()
        {
            var directory = new DirectoryInfo(Constants.ExcelLogDirectory);

            foreach (var fileInfo in directory.GetFiles())
            {
                fileInfo.Delete();
            }

            return new EmptyResult();
        }
    }
}