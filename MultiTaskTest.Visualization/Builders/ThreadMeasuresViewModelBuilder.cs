﻿using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Visualization.Models;

namespace MultiTaskTest.Visualization.Builders
{
    internal class ThreadMeasuresViewModelBuilder
    {
        private readonly ThreadsMeasures _threadsMeasures;
        private readonly ThreadMeasuresViewModel _threadMeasuresViewModel;

        internal ThreadMeasuresViewModelBuilder(ThreadsMeasures threadsMeasures)
        {
            _threadsMeasures = threadsMeasures;

            _threadMeasuresViewModel = new ThreadMeasuresViewModel
            {
                ElapsedTime = threadsMeasures.ElapsedMilliseconds,
                ComputationStatistic = threadsMeasures.CalculationsPerThread,
                ThreadsCount = threadsMeasures.ThreadsCount
            };
        }

        internal static ThreadMeasuresViewModelBuilder CreateFrom(ThreadsMeasures threadsMeasures)
        {
            return new ThreadMeasuresViewModelBuilder(threadsMeasures);
        }

        internal ThreadMeasuresViewModelBuilder WithShowThreadRace(bool showThreadRace)
        {
            _threadMeasuresViewModel.ShowThreadRace = showThreadRace;

            return this;
        }

        internal ThreadMeasuresViewModelBuilder WithThreadMetrics()
        {
            foreach (var threadMetric in _threadsMeasures.ThreadsMetrics)
            {
                ThreadMetricViewModel threadMetricViewModel = ThreadMetricViewModelBuilder
                    .CreateFrom(threadMetric)
                    .WithTimeStamps()
                    .Model();

                _threadMeasuresViewModel.ThreadMetricViewModels.Add(threadMetricViewModel);
            }

            return this;
        }

        internal ThreadMeasuresViewModel Model()
        {
            return _threadMeasuresViewModel;
        }
    }
}