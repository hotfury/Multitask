﻿using System.Collections.Generic;
using System.Linq;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Visualization.Models;
using MultiTaskTest.Visualization.Static;

namespace MultiTaskTest.Visualization.Builders
{
    internal class ThreadMetricViewModelBuilder
    {
        private readonly ThreadMetric _threadMetric;
        private readonly ThreadMetricViewModel _threadMetricViewModel;

        internal static ThreadMetricViewModelBuilder CreateFrom(ThreadMetric threadMetric)
        {
            return new ThreadMetricViewModelBuilder(threadMetric);
        }

        internal ThreadMetricViewModelBuilder(ThreadMetric threadMetric)
        {
            _threadMetric = threadMetric;
            _threadMetricViewModel = new ThreadMetricViewModel
            {
                ThreadName = threadMetric.Name,
                ThreadId = threadMetric.Id,
                IsThreadFromPool = threadMetric.IsThreadFromPool,
                TimeBetweenIterations = new List<TimeStamp>
                {
                    threadMetric.TimeStamps.First()
                }
            };
        }

        internal ThreadMetricViewModelBuilder WithTimeStamps()
        {
            for (int i = 1; i < _threadMetric.TimeStamps.Count; i++)
            {
                double timeBetweenIterations =
                    _threadMetric.TimeStamps[i].IterationTimeStamp -
                    _threadMetric.TimeStamps[i - 1].IterationTimeStamp;

                _threadMetricViewModel.TimeBetweenIterations.Add(new TimeStamp
                {
                    IterationTimeStamp = timeBetweenIterations,
                    ActionId = _threadMetric.TimeStamps[i].ActionId
                });
            }

            int iterationsCount = _threadMetricViewModel.TimeBetweenIterations.Count;
            if (iterationsCount > Constants.ApproximationLimit)
            {
                Approximate(iterationsCount);
            }

            return this;
        }

        internal ThreadMetricViewModel Model()
        {
            return _threadMetricViewModel;
        }

        private void Approximate(int iterationsCount)
        {
            int chunkSize = iterationsCount / Constants.ApproximationLimit;
            List<TimeStamp> approximatedTimeStamps = new List<TimeStamp>();
            for (int i = 0; i < Constants.ApproximationLimit; i++)
            {
                int offset = i * chunkSize;
                List<TimeStamp> chunk = _threadMetricViewModel
                    .TimeBetweenIterations
                    .Skip(offset)
                    .ToList();
                if (chunk.Count >= chunkSize * 2)
                {
                    chunk = chunk.Take(chunkSize).ToList();
                }

                double averageTimeBetweenIterations = chunk.Average(x => x.IterationTimeStamp);
                approximatedTimeStamps.Add(new TimeStamp
                {
                    ActionId = chunk.First().ActionId,
                    IterationTimeStamp = averageTimeBetweenIterations
                });
            }

            _threadMetricViewModel.TimeBetweenIterations = approximatedTimeStamps;
        }
    }
}