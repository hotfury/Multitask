FROM microsoft/aspnetcore:2.0 AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /src
COPY ["MultiTaskTest.Visualization/MultiTaskTest.Visualization.csproj", "MultiTaskTest.Visualization/"]
COPY ["MultiTaskTest/MultiTaskTest.csproj", "MultiTaskTest/"]
RUN dotnet restore "MultiTaskTest.Visualization/MultiTaskTest.Visualization.csproj"
COPY . .
WORKDIR "/src/MultiTaskTest.Visualization"
RUN dotnet build "MultiTaskTest.Visualization.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "MultiTaskTest.Visualization.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "MultiTaskTest.Visualization.dll"]