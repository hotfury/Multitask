﻿namespace MultiTaskTest.TaskFactory
{
    public class TaskFactorySettings
    {
        public static int TasksCount { get; set; }

        public static int TasksChunkSize { get; set; }
    }
}