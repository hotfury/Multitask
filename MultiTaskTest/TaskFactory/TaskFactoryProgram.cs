﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.TaskFactory
{
    public class TaskFactoryProgram : ChunkProgramBase
    {
        public TaskFactoryProgram(string applicationType) : base("TaskFactory", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            var tasks = new List<Task>();
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            for (int i = 0; i < TaskFactorySettings.TasksCount; i++)
            {
                int offset = TaskFactorySettings.TasksChunkSize * i;
                int actionId = i + 1;
                tasks.Add(Task.Factory.StartNew(() =>
                {
                   RunCalculationChunk(bigList, offset, TaskFactorySettings.TasksChunkSize, actionId, threadIterationTicks);
                }, TaskCreationOptions.LongRunning));
            }
            Task.WaitAll(tasks.ToArray());

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            TaskFactorySettings.TasksCount = Convert.ToInt32(args[index]);
            SetChunkSize();
            SetDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            TaskFactorySettings.TasksCount = settings.ActionsCount;
            SetChunkSize();
            SetDescription();
        }

        private void SetChunkSize()
        {
            TaskFactorySettings.TasksChunkSize = LogicSettings.IterationsCount / TaskFactorySettings.TasksCount;
        }

        private void SetDescription()
        {
            ApplicationDescription += $"Tasks count: {TaskFactorySettings.TasksCount}";
        }
    }
}