﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.LinqForAll
{
    public class LinqForAllProgram : ProgramBase
    {
        public LinqForAllProgram(string applicationType) : base("LinqForAll", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            Action<int> action = item =>
            {
                var ticksOnIteration = CalculationService.LongRunningCalculation(item);

                if (LogicSettings.TrackThreadStatistic)
                {
                    threadIterationTicks.Add(new ThreadIterationTicks
                    {
                        TicksOnIteration = ticksOnIteration,
                        ThreadId = ThreadHelper.GetThreadId,
                        ActionId = ThreadHelper.GetThreadId,
                        IsThreadFromPool = ThreadHelper.IsThreadFromPool
                    });
                }
            };

            if (LinqForAllSettings.MaxDegreeOfParallelism != 0)
            {
                bigList.AsParallel()
                    .WithDegreeOfParallelism(LinqForAllSettings.MaxDegreeOfParallelism)
                    .ForAll(action);
            }
            else
            {
                bigList.AsParallel().ForAll(action);
            }

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            LinqForAllSettings.MaxDegreeOfParallelism = Convert.ToInt32(args[index]);
            SetNameAndDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            LinqForAllSettings.MaxDegreeOfParallelism = settings.MaxDegreeOfParallelism;
            SetNameAndDescription();
        }

        private void SetNameAndDescription()
        {
            if (LinqForAllSettings.MaxDegreeOfParallelism != 0)
            {
                ApplicationName += "WithDegree";
                ApplicationDescription += $"Degree of parallelism: {LinqForAllSettings.MaxDegreeOfParallelism}";
            }
            else
            {
                ApplicationDescription += "Degree of parallelism: default";
            }
        }
    }
}