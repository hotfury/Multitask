namespace MultiTaskTest.LinqForAll;

public class LinqForAllSettings
{
    public static int MaxDegreeOfParallelism { get; set; }
}