﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.ParallelInvoke
{
    public class ParallelInvokeProgram : ChunkProgramBase
    {
        public ParallelInvokeProgram(string applicationType) : base("ParallelInvoke", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            List<Action> actions = new List<Action>();
            for (int i = 0; i < ParallelInvokeSettings.ParallelActionsCount; i++)
            {
                int offset = ParallelInvokeSettings.ParallelChunkSize * i;
                int actionId = i + 1;
                actions.Add(() =>
                {
                    RunCalculationChunk(bigList, offset, ParallelInvokeSettings.ParallelChunkSize, actionId, threadIterationTicks);
                });
            }

            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            if (ParallelInvokeSettings.MaxDegreeOfParallelism != 0)
            {
                var parallelOptions = new ParallelOptions
                {
                    MaxDegreeOfParallelism = ParallelInvokeSettings.MaxDegreeOfParallelism
                };
                Parallel.Invoke(parallelOptions, actions.ToArray());
            }
            else
            {
                Parallel.Invoke(actions.ToArray());
            }

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            ParallelInvokeSettings.ParallelActionsCount = Convert.ToInt32(args[index]);
            ParallelInvokeSettings.MaxDegreeOfParallelism = Convert.ToInt32(args[++index]);
            SetChunkSize();
            SetNameAndDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            ParallelInvokeSettings.ParallelActionsCount = settings.ActionsCount;
            ParallelInvokeSettings.MaxDegreeOfParallelism = settings.MaxDegreeOfParallelism;
            SetChunkSize();
            SetNameAndDescription();
        }

        private void SetChunkSize()
        {
            ParallelInvokeSettings.ParallelChunkSize =
                LogicSettings.IterationsCount / ParallelInvokeSettings.ParallelActionsCount;
        }

        private void SetNameAndDescription()
        {
            ApplicationDescription += $"Actions count: {ParallelInvokeSettings.ParallelActionsCount}\n";

            if (ParallelInvokeSettings.MaxDegreeOfParallelism != 0)
            {
                ApplicationName += "WithDegree";
                ApplicationDescription += $"Degree of parallelism: {ParallelInvokeSettings.MaxDegreeOfParallelism}";
            }
            else
            {
                ApplicationDescription += "Degree of parallelism: default";
            }
        }
    }
}