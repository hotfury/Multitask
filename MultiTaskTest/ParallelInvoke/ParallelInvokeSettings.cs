﻿namespace MultiTaskTest.ParallelInvoke
{
    public static class ParallelInvokeSettings
    {
        public static int ParallelActionsCount { get; set; }

        public static int ParallelChunkSize { get; set; }

        public static int MaxDegreeOfParallelism { get; set; }
    }
}