﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.ThreadPool
{
    public class ThreadPoolProgram : ChunkProgramBase
    {
        public ThreadPoolProgram(string applicationType) : base("ThreadPool", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            WaitHandle[] waitHandles = new WaitHandle[ThreadPoolSettings.ActionsCount];
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            for (int i = 0; i < ThreadPoolSettings.ActionsCount; i++)
            {
                int offset = i * ThreadPoolSettings.PoolThreadChunkSize;
                int actionId = i + 1;
                var handle = new EventWaitHandle(false, EventResetMode.AutoReset);
                System.Threading.ThreadPool.QueueUserWorkItem(state =>
                {
                    RunCalculationChunk(bigList, offset, ThreadPoolSettings.PoolThreadChunkSize, actionId, threadIterationTicks);
                    handle.Set();
                }, null);
                waitHandles[i] = handle;
            }
            WaitHandle.WaitAll(waitHandles);

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            ThreadPoolSettings.ActionsCount = Convert.ToInt32(args[index]);
            SetChunkSize();
            SetDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            ThreadPoolSettings.ActionsCount = settings.ActionsCount;
            SetChunkSize();
            SetDescription();
        }

        private void SetChunkSize()
        {
            ThreadPoolSettings.PoolThreadChunkSize = LogicSettings.IterationsCount / ThreadPoolSettings.ActionsCount;
        }

        private void SetDescription()
        {
            ApplicationDescription += $"Actions count: {ThreadPoolSettings.ActionsCount}";
        }
    }
}