﻿namespace MultiTaskTest.ThreadPool
{
    public static class ThreadPoolSettings
    {
        public static int ActionsCount { get; set; }

        public static int PoolThreadChunkSize { get; set; }
    }
}