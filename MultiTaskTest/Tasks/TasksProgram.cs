﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.Tasks
{
    public class TasksProgram : ChunkProgramBase
    {
        public TasksProgram(string applicationType) : base("Tasks", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            var tasks = new List<Task>();
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            for (int i = 0; i < TasksSettings.TasksCount; i++)
            {
                int offset = TasksSettings.TasksChunkSize * i;
                int actionId = i + 1;
                tasks.Add(Task.Run(() =>
                {
                   RunCalculationChunk(bigList, offset, TasksSettings.TasksChunkSize, actionId, threadIterationTicks);
                }));
            }
            Task.WaitAll(tasks.ToArray());

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            TasksSettings.TasksCount = Convert.ToInt32(args[index]);
            SetChunkSize();
            SetDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            TasksSettings.TasksCount = settings.ActionsCount;
            SetChunkSize();
            SetDescription();
        }

        private void SetChunkSize()
        {
            TasksSettings.TasksChunkSize = LogicSettings.IterationsCount / TasksSettings.TasksCount;
        }

        private void SetDescription()
        {
            ApplicationDescription += $"Tasks count: {TasksSettings.TasksCount}";
        }
    }
}