﻿namespace MultiTaskTest.Tasks
{
    public class TasksSettings
    {
        public static int TasksCount { get; set; }

        public static int TasksChunkSize { get; set; }
    }
}