﻿using System.Threading;

namespace MultiTaskTest.Threads
{
    public static class ThreadsSettings
    {
        public static int ThreadsCount { get; set; }

        public static int ThreadsChunkSize { get; set; }

        public static ThreadPriority ThreadPriority { get; set; }
    }
}