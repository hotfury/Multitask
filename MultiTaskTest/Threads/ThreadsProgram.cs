﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.Threads
{
    public class ThreadsProgram : ChunkProgramBase
    {
        public ThreadsProgram(string applicationType) : base("Threads", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            WaitHandle[] waitHandles = new WaitHandle[ThreadsSettings.ThreadsCount];
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            for (int i = 0; i < ThreadsSettings.ThreadsCount; i++)
            {
                int offset = i * ThreadsSettings.ThreadsChunkSize;
                int actionId = i + 1;
                var handle = new EventWaitHandle(false, EventResetMode.AutoReset);
                var thread = new Thread(() =>
                {
                    RunCalculationChunk(bigList, offset, ThreadsSettings.ThreadsChunkSize, actionId, threadIterationTicks);
                    handle.Set();
                });
                waitHandles[i] = handle;
                thread.Priority = ThreadsSettings.ThreadPriority;
                thread.Start();
            }
            WaitHandle.WaitAll(waitHandles);

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            ThreadsSettings.ThreadsCount = Convert.ToInt32(args[index]);
            ThreadsSettings.ThreadPriority = args[index + 1] != "default" 
                ? Enum.Parse<ThreadPriority>(args[index + 1]) 
                : ThreadPriority.Normal;

            SetChunkSize();
            SetNameAndDescription();

            return index + 2;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            ThreadsSettings.ThreadPriority = settings.ThreadPriority;
            ThreadsSettings.ThreadsCount = settings.ActionsCount;
            SetChunkSize();
            SetNameAndDescription();
        }

        private void SetChunkSize()
        {
            ThreadsSettings.ThreadsChunkSize = LogicSettings.IterationsCount / ThreadsSettings.ThreadsCount;
        }

        private void SetNameAndDescription()
        {
            if (ThreadsSettings.ThreadPriority != ThreadPriority.Normal)
            {
                ApplicationName += "WithPriority";
            }

            ApplicationDescription += $"Threads count: {ThreadsSettings.ThreadsCount}\n" +
                                      $"Priority: {ThreadsSettings.ThreadPriority}";
        }
    }
}