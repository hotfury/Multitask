﻿using System;
using System.IO;
using System.Linq;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Statistic;
using OfficeOpenXml;

namespace MultiTaskTest.Logic
{
    internal class Excel : IDisposable
    {
        private const string ExcelTemplateFileName = "Template.xlsx";
        private const string WorkSheetName = "Results";
        private const string MarkerSuffix = "_marker";
        private const string ExcelExtension = ".xlsx";

        private readonly ExcelPackage _excelFile;
        private readonly ExcelWorksheet _excelWorksheet;

        internal Excel(string excelTemplatePath, string applicationType)
        {
            excelTemplatePath = excelTemplatePath is null
                ? string.Empty
                : excelTemplatePath;

            var resultFilePath = Path.Combine(
                excelTemplatePath,
                Constants.ExcelLogDirectory,
                $"{applicationType}-" +
                $"{LogicSettings.IterationsCount}iterations-" +
                $"{LogicSettings.PowerValue}power{ExcelExtension}");

            if (!File.Exists(resultFilePath))
            {
                string newDirectoryPath = Path.Combine(excelTemplatePath, Constants.ExcelLogDirectory);
                string excelTemplateFilePath = Path.Combine(excelTemplatePath, ExcelTemplateFileName);

                Directory.CreateDirectory(newDirectoryPath);
                File.Copy(excelTemplateFilePath, resultFilePath);
            }

            var fileInfo = new FileInfo(resultFilePath);
            _excelFile = new ExcelPackage(fileInfo);
            _excelWorksheet = _excelFile.Workbook.Worksheets[WorkSheetName];
        }

        public void Log(string applicationName, string applicationDescription, ThreadsMeasures threadsMeasures)
        {
            ExcelRangeBase titleCell = _excelWorksheet.Cells.FirstOrDefault(x => x.Text == $"{{{applicationName}}}");
            if (titleCell != null)
            {
                _excelWorksheet.Cells[titleCell.Address].Value = $"{applicationName}\n{applicationDescription}";
            }

            ExcelRangeBase cell = _excelWorksheet.Cells.First(x => x.Text == $"{{{applicationName}{MarkerSuffix}}}");
            int row = cell.Start.Row;
            int column = cell.Start.Column;
            _excelWorksheet.Cells[row + 1, column].Value = _excelWorksheet.Cells[row, column].Text;
            _excelWorksheet.Cells[row, column].Value = threadsMeasures.ElapsedMilliseconds;
            _excelFile.Save();
        }

        public void Dispose()
        {
            _excelFile.Dispose();
        }
    }
}