﻿namespace MultiTaskTest.Logic
{
    public static class LogicSettings
    {
        public static int IterationsCount { get; set; }

        public static int PowerValue { get; set; }

        public static bool TrackThreadStatistic { get; set; }

        public static int MinThreadsInPool { get; set; }

        public static int ScaleTicksValue { get; set; } = 1;
    }
}