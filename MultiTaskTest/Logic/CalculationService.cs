﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace MultiTaskTest.Logic
{
    public class CalculationService
    {
        private const int MinValue = 100;
        private const int MaxValue = 200;

        public long LongRunningCalculation(int element)
        {
            var ticks = DateTime.UtcNow.Ticks;
            BigInteger.Pow(element, LogicSettings.PowerValue);

            return ticks;
        }

        public List<int> InitializeList()
        {
            var bigList = new List<int>();
            for (int i = 0; i < LogicSettings.IterationsCount; i++)
            {
                bigList.Add(new Random().Next(MinValue, MaxValue));
            }

            return bigList;
        }
    }
}