﻿using System.Collections.Generic;
using MultiTaskTest.Common.Program;
using MultiTaskTest.LinqForAll;
using MultiTaskTest.ParallelFor;
using MultiTaskTest.ParallelForEach;
using MultiTaskTest.ParallelInvoke;
using MultiTaskTest.Synchronous;
using MultiTaskTest.TaskFactory;
using MultiTaskTest.Tasks;
using MultiTaskTest.ThreadPool;
using MultiTaskTest.Threads;

namespace MultiTaskTest.Common
{
    public class ApplicationMap
    {
        public Dictionary<string, ProgramBase> Applications { get; }

        public ApplicationMap(string applicationType)
        {
            Applications = new Dictionary<string, ProgramBase>
            {
                { "LinqForAll", new LinqForAllProgram(applicationType) },
                { "ParallelFor", new ParallelForProgram(applicationType) },
                { "ParallelForEach", new ParallelForEachProgram(applicationType) },
                { "ParallelInvoke", new ParallelInvokeProgram(applicationType) },
                { "Tasks", new TasksProgram(applicationType) },
                { "ThreadPool", new ThreadPoolProgram(applicationType) },
                { "Threads", new ThreadsProgram(applicationType) },
                { "TaskFactory", new TaskFactoryProgram(applicationType) },
                { "Synchronous", new SynchronousProgram(applicationType) }
            };
        }
    }
}