﻿using System.Collections.Generic;

namespace MultiTaskTest.Common.Statistic
{
    public class ThreadMetric
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public bool IsThreadFromPool { get; set; }

        public IList<TimeStamp> TimeStamps { get; set; }

        public ThreadMetric()
        {
            TimeStamps = new List<TimeStamp>();
        }
    }
}