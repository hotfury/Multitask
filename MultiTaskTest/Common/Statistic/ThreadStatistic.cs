﻿using System.Collections.Generic;

namespace MultiTaskTest.Common.Statistic
{
    public class ThreadStatistic
    {
        public long StartPoint { get; set; }

        public IEnumerable<ThreadIterationTicks> ThreadIterationTicks { get; set; }
    }
}