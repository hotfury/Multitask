﻿namespace MultiTaskTest.Common.Statistic
{
    public class ThreadIterationTicks
    {
        public int ThreadId { get; set; }

        public int ActionId { get; set; }

        public bool IsThreadFromPool { get; set; }

        public long TicksOnIteration { get; set; }
    }
}