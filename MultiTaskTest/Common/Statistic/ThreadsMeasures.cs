﻿using System.Collections.Generic;

namespace MultiTaskTest.Common.Statistic
{
    public class ThreadsMeasures
    {
        public IList<ThreadMetric> ThreadsMetrics { get; set; }

        public long ElapsedMilliseconds { get; set; }

        public int ThreadsCount { get; set; }

        public Dictionary<string, int> CalculationsPerThread { get; set; }

        public ThreadsMeasures()
        {
            ThreadsMetrics = new List<ThreadMetric>();
            CalculationsPerThread = new Dictionary<string, int>();
        }
    }
}