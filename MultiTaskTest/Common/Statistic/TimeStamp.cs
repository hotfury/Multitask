﻿namespace MultiTaskTest.Common.Statistic
{
    public class TimeStamp
    {
        public double IterationTimeStamp { get; set; }

        public int ActionId { get; set; }
    }
}