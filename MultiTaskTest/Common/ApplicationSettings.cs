﻿using System.Threading;

namespace MultiTaskTest.Common
{
    public class ApplicationSettings
    {
        public string ApplicationName { get; set; }

        public int IterationsCount { get; set; }

        public int PowerValue { get; set; }

        public int ThreadPoolMinimum { get; set; }

        public int ScaleTicksValue {get; set; }

        public int ActionsCount { get; set; }

        public int MaxDegreeOfParallelism { get; set; }

        public bool ShowThreadRace { get; set; }

        public bool TrackThreadStatistic { get; set; } = true;

        public string ExcelLogPath { get; set; }

        public ThreadPriority ThreadPriority { get; set; }
    }
}