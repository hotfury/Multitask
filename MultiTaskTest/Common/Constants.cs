﻿namespace MultiTaskTest.Common
{
    public class Constants
    {
        public const string WebApplicationType = "Web";

        public const string ConsoleApplicationType = "Console";

        public const string ExcelLogDirectory = "ExcelLogs";
    }
}