﻿using System.Threading;

namespace MultiTaskTest.Common
{
    public static class ThreadHelper
    {
        public static int GetThreadId => Thread.CurrentThread.ManagedThreadId;

        public static bool IsThreadFromPool => Thread.CurrentThread.IsThreadPoolThread;

        public static int ThreadPoolSize
        {
            get
            {
                System.Threading.ThreadPool.GetAvailableThreads(out int availableThreads, out int availableStreams);

                return availableThreads;
            }
        }

        public static int MinimumThreadPoolSize
        {
            get
            {
                System.Threading.ThreadPool.GetMinThreads(out int availableThreads, out int availableStreams);

                return availableThreads;
            }
        }
    }
}