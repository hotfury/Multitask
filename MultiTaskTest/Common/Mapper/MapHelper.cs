﻿using System.Linq;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.Common.Mapper
{
    public class MapHelper
    {
        private readonly ThreadStatistic _threadsStatistic;

        public MapHelper(ThreadStatistic threadsStatistic)
        {
            _threadsStatistic = threadsStatistic;
        }

        public static MapHelper From(ThreadStatistic threadStatistic)
        {
            return new MapHelper(threadStatistic);
        }

        public ThreadsMeasures To(ThreadsMeasures threadsMeasures)
        {
            var groupedAndOrderedTicks = _threadsStatistic.ThreadIterationTicks
                .OrderBy(x => x.TicksOnIteration)
                .GroupBy(x => x.ThreadId);

            foreach (var groupedAndOrderedTick in groupedAndOrderedTicks)
            {
                var threadMetric = new ThreadMetric
                {
                    Id = groupedAndOrderedTick.Key,
                    Name = ThreadHelper.GetThreadId == groupedAndOrderedTick.Key
                        ? $"Main #{groupedAndOrderedTick.Key}"
                        : $"Thread #{groupedAndOrderedTick.Key}",
                    IsThreadFromPool = groupedAndOrderedTick.First().IsThreadFromPool
                };


                foreach (var threadIterationTick in groupedAndOrderedTick)
                {
                    long iterationTimeStamp =
                        (threadIterationTick.TicksOnIteration - _threadsStatistic.StartPoint) / LogicSettings.ScaleTicksValue;
                    threadMetric.TimeStamps.Add(new TimeStamp
                    {
                        IterationTimeStamp = iterationTimeStamp,
                        ActionId = threadIterationTick.ActionId
                    });
                }

                threadsMeasures.ThreadsMetrics.Add(threadMetric);
            }

            threadsMeasures.ThreadsCount = _threadsStatistic
                .ThreadIterationTicks
                .Select(x => x.ThreadId)
                .Distinct().Count();

            foreach (var threadMetric in threadsMeasures.ThreadsMetrics)
            {
                threadsMeasures.CalculationsPerThread.Add(threadMetric.Name, threadMetric.TimeStamps.Count);
            }

            return threadsMeasures;
        }
    }
}