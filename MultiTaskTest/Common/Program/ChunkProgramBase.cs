﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.Common.Program
{
    public abstract class ChunkProgramBase : ProgramBase
    {
        protected ChunkProgramBase(string applicationName, string applicationType) : 
            base(applicationName, applicationType)
        {}

        protected void RunCalculationChunk(
            List<int> bigList,
            int offset,
            int chunkSize,
            int actionId,
            ConcurrentBag<ThreadIterationTicks> threadIterationTicks)
        {
            List<int> chunk = bigList.Skip(offset).ToList();
            if (chunk.Count >= chunkSize * 2)
            {
                chunk = chunk.Take(chunkSize).ToList();
            }

            foreach (var item in chunk)
            {
                var ticksOnIteration = CalculationService.LongRunningCalculation(item);

                if (LogicSettings.TrackThreadStatistic)
                {
                    threadIterationTicks.Add(new ThreadIterationTicks
                    {
                        TicksOnIteration = ticksOnIteration,
                        ThreadId = ThreadHelper.GetThreadId,
                        ActionId =  actionId,
                        IsThreadFromPool = ThreadHelper.IsThreadFromPool
                    });
                }
            }
        }
    }
}