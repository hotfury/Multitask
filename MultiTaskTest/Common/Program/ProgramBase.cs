﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MultiTaskTest.Common.Mapper;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.Common.Program
{
    public abstract class ProgramBase
    {
        private string _excelTemplatePath;

        protected readonly CalculationService CalculationService = new CalculationService();

        protected string ApplicationName { get; set; }

        protected string ApplicationType { get; set; }

        protected string ApplicationDescription { get; set; } = string.Empty;

        protected ProgramBase(string applicationName, string applicationType)
        {
            ApplicationName = applicationName;
            ApplicationType = applicationType;
        }

        public ThreadsMeasures Main(ApplicationSettings settings)
        {
            InitializeSettings(settings);

            ThreadsMeasures threadsMeasures = Main();

            return threadsMeasures;
        }

        public void Main(string[] args, int startPosition)
        {
            InitializeSettings(args, startPosition);

            ThreadsMeasures threadsMeasures = Main();

            if (LogicSettings.TrackThreadStatistic)
            {
                Console.WriteLine($"Threads count: {threadsMeasures.ThreadsCount}");

                foreach (var item in threadsMeasures.CalculationsPerThread)
                {
                    string threadType = threadsMeasures.ThreadsMetrics.First(x => x.Name == item.Key).IsThreadFromPool
                        ? "from pool"
                        : "new";

                    Console.WriteLine($"{item.Key}({threadType}) calculated {item.Value} times");
                }
            }
        }

        protected virtual void InitializeSettings(ApplicationSettings settings)
        {
            LogicSettings.IterationsCount = settings.IterationsCount;
            LogicSettings.PowerValue = settings.PowerValue;
            LogicSettings.TrackThreadStatistic = settings.TrackThreadStatistic;
            LogicSettings.MinThreadsInPool = settings.ThreadPoolMinimum;
            LogicSettings.ScaleTicksValue = settings.ScaleTicksValue;
            _excelTemplatePath = settings.ExcelLogPath;
        }

        protected virtual int InitializeSettings(string[] args, int startPosition)
        {
            LogicSettings.IterationsCount = Convert.ToInt32(args[startPosition++]);
            LogicSettings.PowerValue = Convert.ToInt32(args[startPosition++]);
            LogicSettings.TrackThreadStatistic = Convert.ToBoolean(args[startPosition++]);
            string threadPoolMinimum = args[startPosition++];
            if (string.Compare(threadPoolMinimum, "default", StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                LogicSettings.MinThreadsInPool = Convert.ToInt32(threadPoolMinimum);
            }
            _excelTemplatePath = $"{args[startPosition++]}";

            return startPosition;
        }

        protected abstract ThreadStatistic PerformCalculation(List<int> bigList);

        private ThreadsMeasures Main()
        {
            Console.WriteLine($"{ApplicationName} started");
            Console.WriteLine(ApplicationDescription);
            Console.WriteLine($"CPU count(Pool minimum): {ThreadHelper.MinimumThreadPoolSize}");
            Console.WriteLine($"Pool size: {ThreadHelper.ThreadPoolSize}");

            if (LogicSettings.MinThreadsInPool != 0)
            {
                System.Threading.ThreadPool.SetMinThreads(LogicSettings.MinThreadsInPool, LogicSettings.MinThreadsInPool);
            }

            List<int> bigList = CalculationService.InitializeList();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var threadsStatistic = PerformCalculation(bigList);
            stopwatch.Stop();

            var threadsMeasures = new ThreadsMeasures
            {
                ElapsedMilliseconds = stopwatch.ElapsedMilliseconds
            };

            if (LogicSettings.TrackThreadStatistic)
            {
                MapHelper.From(threadsStatistic).To(threadsMeasures);
            }

            if (_excelTemplatePath is not null)
            {
                using var excel = new Excel(_excelTemplatePath, ApplicationType);
                excel.Log(ApplicationName, ApplicationDescription, threadsMeasures);
            }

            Console.WriteLine($"Timing: {threadsMeasures.ElapsedMilliseconds}");

            return threadsMeasures;
        }
    }
}