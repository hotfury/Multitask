﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.Synchronous
{
    public class SynchronousProgram : ProgramBase
    {
        public SynchronousProgram(string applicationType) : base("Synchronous", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            foreach (var item in bigList)
            {
                var ticksOnIteration = CalculationService.LongRunningCalculation(item);

                if (LogicSettings.TrackThreadStatistic)
                {
                    threadIterationTicks.Add(new ThreadIterationTicks
                    {
                        TicksOnIteration = ticksOnIteration,
                        ThreadId = ThreadHelper.GetThreadId,
                        IsThreadFromPool = ThreadHelper.IsThreadFromPool
                    });
                }
            }

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }
    }
}