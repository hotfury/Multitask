﻿namespace MultiTaskTest.ParallelFor
{
    public static class ParallelForSettings
    {
        public static int MaxDegreeOfParallelism { get; set; }
    }
}