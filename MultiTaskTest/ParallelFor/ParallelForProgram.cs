﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.ParallelFor
{
    public class ParallelForProgram : ProgramBase
    {
        public ParallelForProgram(string applicationType) : base("ParallelFor", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            Action<int> action = index =>
            {
                var ticksOnIteration = CalculationService.LongRunningCalculation(bigList[index]);

                if (LogicSettings.TrackThreadStatistic)
                {
                    threadIterationTicks.Add(new ThreadIterationTicks
                    {
                        TicksOnIteration = ticksOnIteration,
                        ThreadId = ThreadHelper.GetThreadId,
                        ActionId = ThreadHelper.GetThreadId,
                        IsThreadFromPool = ThreadHelper.IsThreadFromPool
                    });
                }
            };

            if (ParallelForSettings.MaxDegreeOfParallelism != 0)
            {
                var parallelOptions = new ParallelOptions
                {
                    MaxDegreeOfParallelism = ParallelForSettings.MaxDegreeOfParallelism
                };
                Parallel.For(0, bigList.Count, parallelOptions, action);
            }
            else
            {
                Parallel.For(0, bigList.Count, action);
            }

            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            ParallelForSettings.MaxDegreeOfParallelism = Convert.ToInt32(args[index]);
            SetNameAndDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            ParallelForSettings.MaxDegreeOfParallelism = settings.MaxDegreeOfParallelism;
            SetNameAndDescription();
        }

        private void SetNameAndDescription()
        {
            if (ParallelForSettings.MaxDegreeOfParallelism != 0)
            {
                ApplicationName += "WithDegree";
                ApplicationDescription += $"Degree of parallelism: {ParallelForSettings.MaxDegreeOfParallelism}";
            }
            else
            {
                ApplicationDescription += "Degree of parallelism: default";
            }
        }
    }
}