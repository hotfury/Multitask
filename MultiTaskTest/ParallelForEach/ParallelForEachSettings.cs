﻿namespace MultiTaskTest.ParallelForEach
{
    public static class ParallelForEachSettings
    {
        public static int MaxDegreeOfParallelism { get; set; }
    }
}