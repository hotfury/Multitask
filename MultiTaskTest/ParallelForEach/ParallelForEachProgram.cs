﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using MultiTaskTest.Common;
using MultiTaskTest.Common.Program;
using MultiTaskTest.Common.Statistic;
using MultiTaskTest.Logic;

namespace MultiTaskTest.ParallelForEach
{
    public class ParallelForEachProgram : ProgramBase
    {
        public ParallelForEachProgram(string applicationType) : base("ParallelForEach", applicationType)
        {}

        protected override ThreadStatistic PerformCalculation(List<int> bigList)
        {
            var threadIterationTicks = new ConcurrentBag<ThreadIterationTicks>();
            var threadStatistic = new ThreadStatistic
            {
                StartPoint = DateTime.UtcNow.Ticks
            };

            Action<int> action = item =>
            {
                var ticksOnIteration = CalculationService.LongRunningCalculation(item);

                if (LogicSettings.TrackThreadStatistic)
                {
                    threadIterationTicks.Add(new ThreadIterationTicks
                    {
                        TicksOnIteration = ticksOnIteration,
                        ThreadId = ThreadHelper.GetThreadId,
                        ActionId = ThreadHelper.GetThreadId,
                        IsThreadFromPool = ThreadHelper.IsThreadFromPool
                    });
                }
            };

            if(ParallelForEachSettings.MaxDegreeOfParallelism != 0)
            {
                var parallelOptions = new ParallelOptions
                {
                    MaxDegreeOfParallelism = ParallelForEachSettings.MaxDegreeOfParallelism
                };
                Parallel.ForEach(bigList, parallelOptions, action);
            }
            else
            {
                Parallel.ForEach(bigList, action);
            }
            
            threadStatistic.ThreadIterationTicks = threadIterationTicks;

            return threadStatistic;
        }

        protected override int InitializeSettings(string[] args, int startPosition)
        {
            int index = base.InitializeSettings(args, startPosition);
            ParallelForEachSettings.MaxDegreeOfParallelism = Convert.ToInt32(args[index]);
            SetNameAndDescription();

            return index + 1;
        }

        protected override void InitializeSettings(ApplicationSettings settings)
        {
            base.InitializeSettings(settings);
            ParallelForEachSettings.MaxDegreeOfParallelism = settings.MaxDegreeOfParallelism;
            SetNameAndDescription();
        }

        private void SetNameAndDescription()
        {
            if (ParallelForEachSettings.MaxDegreeOfParallelism != 0)
            {
                ApplicationName += "WithDegree";
                ApplicationDescription += $"Degree of parallelism: {ParallelForEachSettings.MaxDegreeOfParallelism}";
            }
            else
            {
                ApplicationDescription += "Degree of parallelism: default";
            }
        }
    }
}