﻿using MultiTaskTest.Common;

namespace MultiTaskTest.ConsoleApp
{
    public class Program
    {
        private static readonly ApplicationMap ApplicationMap = new ApplicationMap(Constants.ConsoleApplicationType);

        public static void Main(string[] args)
        {
            int startPosition = 0;
            ApplicationMap.Applications[args[startPosition++]].Main(args, startPosition);
        }
    }
}